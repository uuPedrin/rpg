from art import text2art as arte
from random import randint as aleatorio
from random import choice as escolha
from time import sleep
import os

class Char:
    def __init__(self,name:str="ducky",classe:int=0):
        self.name = name
        if classe == 0:
            self.classe = "Guerreiro"
            self.maxpoder = aleatorio(5,10)
        elif classe == 1:
            self.classe = "Mago"
            self.maxpoder = aleatorio(7,15)
        self.maxvida = 10

    def levelUp(self):
        self.maxvida+= 5
        self.maxpoder+= 5
        pass

class Enemy:
    def __init__(self,reset):
        self.name = escolha(["Slime","Wolf","Boar","Killer Bunny","Psycho Duck"])
        pass

harry = Char("harry",0)
harry.levelUp()
